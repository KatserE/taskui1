# Описание
Данный репозиторий содержит простой проект из нескольких экранов для IOS, созданный
в целях ознакомления со средой разработки и основными моментами построения
приложений, использующих UIKit.

# Задание
1. Необходимо повторить проект из [видео](https://www.youtube.com/watch?v=fevDrVqoCXs).
