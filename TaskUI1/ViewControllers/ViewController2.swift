//
//  ViewController2.swift
//  TaskUI1
//
//  Created by Евгений Кацер on 04.05.2023.
//

import UIKit

class ViewController2: UIViewController {
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didPressBack(_ backButton: UIButton) {
        dismiss(animated: true)
    }
    
    deinit {
         print("Deallocated")
    }
}
